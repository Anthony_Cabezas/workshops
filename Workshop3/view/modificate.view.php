<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Modificar Categoria</title>


    <script>
        $(document).ready(function(){
            $('#modal1').modal();
            $('#modal1').modal('open'); 
        });
    </script>

</head>
<body>

    <!-- Modal Structure -->
    <div id="modal1" class="modal" style="text-align: center;">

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>"  method="post">

            <div class="modal-content" style="text-align: center;">
            <h4>Modificar Categoria : <?php echo $categoria["nombre"];?></h4>
            <input type="text" name="categoria" value="<?php echo $categoria['nombre'];?>">
            <input type="hidden" name="id" value = "<?php echo $categoria['id'];?>">
            <input type="text" name="descripcion" value="<?php echo $categoria['descripcion'];?>">
            <button type="submit" class="btn modal-trigger" title="Editar">Modificar Categoria<i class="material-icons"></i> </button>
            </div>
        
        </form>

    </div>

</body>
</html>