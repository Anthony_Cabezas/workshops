<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

  <title>Categorias</title>
</head>
<body>
<div class="container">
  <?php require ('header.php') ?>
    <h1>Categorias</h1>
    <form action="<?php htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="POST" class="form-inline" role="form">
      <div class="form-group">
        <label class="sr-only" for="">Nombre</label>
        <input type="text" class="form-control" name="nombre" placeholder="Nombre">
      </div>
      <div class="form-group">
        <label class="sr-only" for="">Descripcion</label>
        <input type="text" class="form-control" name="descripcion" placeholder="Descripcion">
      </div>

      <input type="submit" class="btn btn-primary" value="Guardar"></input>
    </form>
</div>

</body>
</html>