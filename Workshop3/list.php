<?php
require 'conexion.php';
  // get all users from the database
  $conexion = conexion();
  $sql = 'SELECT * FROM categorias';
  $result = $conexion->query($sql);
  $categorias = $result->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

    <title>Document</title>
</head>

<body>
    <div class="container">
        <?php require ('header.php') ?>
        <h1>Lista de Categorias</h1>
        <hr>
        <?php foreach($categorias as $categoria):?>
        <div class="col s3 m3">
            <div class="card blue-grey darken-1">
                <div class="card-content white-text">
                    <h2 class="card-title">Categoria: <?php echo $categoria["nombre"];?></h2>
                    <p>Descripcion: <?php echo $categoria["descripcion"];?></p>
                </div>
                <div class="card-action">
                    <a href="modificate.php?id=<?php echo $categoria["id"];?>" class="btn modal-trigger"
                        title="Editar"> <i class="material-icons">Editar</i> </a>

                    <form action="eliminar.php" style="display: inline-block;" method="POST"
                        onsubmit="return confirm('Seguro que desea Eliminar la categoria: <?php echo $categoria['nombre'];?>');">
                        <input type="hidden" name="id" value="<?php echo $categoria["id"];?>">
                        <button type="submit" class="btn modal-trigger" title="Eliminar">delete</button>
                    </form>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</body>

</html