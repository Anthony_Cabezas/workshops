<?php
require 'class/categoria.php';

if($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre = $_POST['nombre'];
    $descripcion = $_POST['descripcion'];

    $cate = new Categoria($nombre, $descripcion);
    $cate->insert();
    header('Location: list.php');
  }

require 'view/index.view.php';