<?php

    require 'class/usuario.php';
    require 'funciones.php';

    $errores = "";

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $usuario = filter_var(trim($_POST['usuario']), FILTER_SANITIZE_STRING);
        $contra = filter_var(trim($_POST['pass']), FILTER_SANITIZE_STRING);
        $nombre = filter_var(trim($_POST['nombre']), FILTER_SANITIZE_STRING);
        $apellido = filter_var(trim($_POST['apellido']), FILTER_SANITIZE_STRING);


            if(verificarUsuario($usuario) !== false){
                $errores .= "<li>El usuario ya existe!</li>";
            }
            else{
                $contra = hash('sha512',$contra);
                $usuario = new Usuario($usuario, $contra, $nombre, $apellido);
                $usuario->insert();
                header("Location: index.php");
            }
    }


require "views/registro.view.php";