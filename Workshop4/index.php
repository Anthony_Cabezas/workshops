<?php session_start();

    require "funciones.php";
    $errores = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $usuario = filter_var(trim($_POST["usuario"]), FILTER_SANITIZE_STRING);
        $contra = filter_var(trim($_POST["pass"]), FILTER_SANITIZE_STRING);
        $contra = hash('sha512',$contra);


        if(existeUsuario($usuario, $contra) === false){
            $errores .= "<li>El Usuario no existe o la información es Incorrecta!</li>";
        }elseif(verificarUsuario($usuario)['tipo'] == 0){
            $_SESSION["nombre"] = verificarUsuario($usuario)['nombre'];
            $_SESSION["apellido"] = verificarUsuario($usuario)['apellido'];
            $_SESSION["tipo"] = verificarUsuario($usuario)['tipo'];
            header("Location: admin.php");
        }
        else{
            $_SESSION["nombre"] = verificarUsuario($usuario)['nombre'];
            $_SESSION["apellido"] = verificarUsuario($usuario)['apellido'];
            $_SESSION["tipo"] = verificarUsuario($usuario)['tipo'];
            header("Location: cliente.php");
        }
    }

require "views/index.view.php";