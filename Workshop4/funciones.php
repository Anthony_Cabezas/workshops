<?php

    require_once 'conexion.php';

    function verificarUsuario($usuario){
        $sql = "SELECT * FROM usuarios WHERE usuario = :usuario";
        $statement = conexion()->prepare($sql);
        $statement->execute(array(':usuario' => $usuario));
        $resultado = $statement->fetch();
        return $resultado;
    }

    function existeUsuario($usuario, $contra){
        $sql = "SELECT * FROM usuarios WHERE usuario = :usuario AND contra = :contra;";
        $statement = conexion()->prepare($sql);
        $statement->execute(array(':usuario' => $usuario,':contra' => $contra));
        $resultado = $statement->fetch();
        return $resultado;
    }
